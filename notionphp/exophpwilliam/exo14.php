
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Table de multiplication</title>
    <style>
    table thead tr th { background:blue; } /* thead : 1ere ligne */
   
    table tbody tr td:first-child { background:blue; } /* tbody : 1ere colonne */
    </style>
</head>
<body>

<table border=1px  width="400">
<?php
$NbrCol 	= 9; // nombre de colonnes
$NbrLigne 	= 9; // nombre de lignes
// --------------------------------------------------------
// on affiche en plus sur les 1ere ligne et 1ere colonne 
// les valeurs a multiplier (dans des cases en couleur)
// le tableau final fera donc 10 x 10
// --------------------------------------------------------
	// 1ere ligne (ligne 0) 
?>
<thead>
	<tr>
		<th></th>
<?php
	for ($j=1; $j<=$NbrCol; $j++) // index de colonne
	{ 
?>
		<th><?php echo $j; ?></th>
<?php
	} 
?>
	</tr>
</thead>

<tbody>
<?php
	// lignes suivantes
	for ($i=1; $i<=$NbrLigne; $i++) // index de ligne
	{ 
?>
	<tr>
<?php
		for ($j=1; $j<=$NbrCol; $j++) // index de colonne
		{
			// 1ere colonne (colonne 0)
			if ($j==1) 
			{ 
?>
		<td><?php echo $i; ?></td>
<?php
			}
			// colonnes suivantes
?>
		<td <?php if ($i==$j) echo ' class="tdij"'; // on ajoute une classe pour les cellules où $i==$j ?>>
<?php
			// -------------------------
			// DONNEES A AFFICHER dans la cellule
			echo $i*$j; // ici : table de multiplication
			// -------------------------
?>
		</td>
<?php
		} 
?>
	</tr>
<?php
		$j = 1; // on ré-initialise le numéro de colonne
	}
?>
</tbody>
</table> 
<!-- écrire le code avant ce commentaire -->

</body>
</html>

