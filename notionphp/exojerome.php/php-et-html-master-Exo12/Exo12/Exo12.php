<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <link href="https://fonts.googleapis.com/css?family=Public+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styleexo12.css">
  
<style>
body{
  background-color:lightgray;
}
h2{
  color:white;
}
img{
  width:50px;
  height:50px;
  }
 
  .container{
    width:70%;
    margin:auto;
    display:flex;
    justify-content:space-between;
  }
  .container div{
    text-align:center;
    background-color:white;
    padding:50px;
    border-radius:10px;
    box-shadow:0 0 5px 5px darkgray;

  }

  #container2 {
    background-color:white;
    text-align:center;
    font-weight:bold;
    width:70%;
    margin:30px auto;
    padding:20px;
    border-radius:10px;
    box-shadow:0 0 5px 5px darkgray;

  }
  table{
    width:70%;
    background-color:white;
    margin: auto;
    border-radius:10px;
    box-shadow:0 0 5px 5px darkgray;
  }
  tr:hover{
    background-color:lightgrey;
    color:white;
    transition:background-color,1.5s;
  }
  td{
    padding:30px;
  }
</style>
</head>  
    <?php
    
      /* 
    *   Simuler le tableau de bord d'une déchetterie grâce à la puissance de calcul de PHP et l'affichage du CSS
    *   Ici, votre tableau de bord, permettera de visualisé les stocks de dechets ( en tonne ) de la communauté de commune
    *   Trois catégories dans le tri : cartons, déchets verts, mobilier. La fonction rand affectera une quantité aléatoire aux  
    *   déchetteries du réseaux, à vous de faire en sorte que l'on sache qui à quoi et combien cela represente en tout. Préciser en 
    *   nombre de camion(s) le volume que cela represente. Un camion = 30 tonnes.
    *   Police utiliser : Public Sans sur Google Font, effet de ligne plus grise au survol de la ligne 
    */
    
    
    $lev = "Loir en vallée";
    $lcsll = "La Chartre-sur-le Loir";
    $bsd = "Beaumont-sur-Dême";
    $m = "Marçon";
    $c = "Chahaignes";
    $l = "Lhomme";
    
    $carton = rand(0,20);
    $vert = rand(0,20);
    $mobilier = rand(0,20);
    $totalcarton=$carton*6;
    $totalvert=$vert*6;
    $totalmobilier=$mobilier*6;
    $totalcamion1=round($totalcarton/30);
    $totalcamion2=round($totalvert/30);
    $totalcamion3 =round($totalmobilier/30);
    $supertotal=$totalcamion1+$totalcamion2+$totalcamion3;   
    ?>
   
    <!-- écrire le code après ce commentaire -->
    
      <h1>SICTOM de Montoire / La Chartre</h1>
    
      <h2>Quantité total de déchets dans la Communauté de communes</h2> 
    
    <div class="container">
        <div>
             <img src="carton.svg">
             <p><?php echo $totalcarton;?></p>
             <h3>Besoin de<?php echo $totalcamion1;?>  camion(s)</h3>
        </div>
        <div>
             <img src="vert.svg">
             <p><?php echo $totalvert;?></p>
             <h3>Besoin de<?php echo $totalcamion2;?>  camion(s)</h3>
        </div>
        <div>
              <img src="mobilier.svg">
              <p><?php echo $totalmobilier?></p>
              <h3>Besoin de<?php echo $totalcamion3;?>  camion(s)</h3>
        </div>

    </div>


    <div id="container2">
        <p>Nombre de camions total à prévoir :<?php echo $supertotal;?> </p>
    </div>
    <table cellspacing=0>

    <thead>
    <tr>
    <td>Site de collecte</td>
    <td><img src="carton.svg"></td>
    <td><img src="vert.svg"></td>
    <td><img src="mobilier.svg"></td>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td>Loir en vallée</td>
    <td><?php echo $carton?></td>
    <td><?php echo $vert?></td>
    <td><?php echo $mobilier?></td>
    </tr>
    <tr>
    <td>La Chartre-sur le Loir</td>
    <td><?php echo $carton?></td>
    <td><?php echo $vert?></td>
    <td><?php echo $mobilier ?></td>
    </tr>
    <tr>
    <td>Beaumont-sur-Dême</td>
    <td><?php echo $carton ?></td>
    <td><?php echo $vert ?></td>
    <td><?php echo $mobilier ?></td>
    </tr>
    <tr>
    <td>Marçon</td>
    <td><?php echo $carton?></td>
    <td><?php echo $vert?></td>
    <td><?php echo $mobilier?></td>
    </tr>
    <tr>
    <td>Chahaignes</td>
    <td><?php echo $carton?></td>
    <td><?php echo $vert?></td>
    <td><?php echo $mobilier?></td>
    </tr>
    <tr>
    <td>Lhomme</td>
    <td><?php echo $carton?></td>
    <td><?php echo $vert?></td>
    <td><?php echo $mobilier?></td>
    </tr>
    </tbody>

    </table>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>

